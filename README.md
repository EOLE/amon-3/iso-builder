# iso-builder

Create the Amon 3 ISO

## Clone all the Amon 3 repository

- Install the [myrepos](https://myrepos.branchable.com/) utility
- Create a root directory for Amon 3
  ```
  mkdir ~/src/amon-3 && cd ~/src/amon-3
  ```
- Clone this repository
  ```
  git clone https://gitlab.mim-libre.fr/EOLE/amon-3/iso-builder.git
  ```
- Put the `myrepos` configuration in place
  ```
  ln -s ~/src/amon-3/iso-builder/.mrconfig ~/src/amon-3/.mrconfig
  ```
- Make sure this configuration file is usable by the `mr` command
  ```
  echo '~/src/amon-3/.mrconfig' >> ~/.mrtrust
  ```
- Clone all the repositories
  ```
  cd ~/src/amon-3 && mr checkout
  ```
